const mongoose = require('mongoose');
const Schema = mongoose.Schema; // define los campos que va a tener tu tabla

var usuariosSchema = new Schema({
    nombre:{
        type: String,
        trim: true,
        lowercase: true
    },
    clave:{
        type: String,
        trim: true
    }
});

module.exports = mongoose.model('usuarios', usuariosSchema);