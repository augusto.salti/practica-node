const Usuarios = require('../modelo/usuario');

exports.nuevoUsuario = async (req,res,next) => {
    //console.log(req.body);
  
    const usuarios = new Usuarios(req.body);  //modelo usuarios
    // req.body => recibe el nombre y la clave
  
    try {
      await usuarios.save() // si lo recibe
      res.json({ mensaje : 'Se ha agregado un nuevo usuario correctamente!' });
    } catch (error) {
      console.log(error);
      next();
    }
  }

  exports.mostrarUsuarios = async (req,res,next) => {

    try {
    const usuarios = await Usuarios.find({});
      res.json(usuarios);
    } catch (error) {
      console.log(error);
      next();
    }
  }

  exports.mostrarUsuario = async (req,res,next) => {
    const usuarios = await Usuarios.findById(req.params.id);
  
    if(!usuarios){
      res.json({mensaje: "El usuario no existe"});
    }
  
  //Mostrar cliente
    res.json({usuarios});
  }