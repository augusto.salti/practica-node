const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./rutas');
const mongoose = require('mongoose');

//Conectar mongo usando mongoose
mongoose.connect('mongodb://localhost:27017/db_usuarios', {useNewUrlParser: true, useUnifiedTopology: true});


const app = new express();


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use('/', routes());




app.listen(5000);