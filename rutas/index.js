const express = require('express');
const usuarioControlador = require('../controlador/usuarioControlador');
const router = express.Router();

module.exports = function(){
 // USUARIOS // 

    // AGREGAR USUARIOS
    router.post('/usuarios', usuarioControlador.nuevoUsuario);

    // MOSTRAR USUARIOS
    router.get('/usuarios', usuarioControlador.mostrarUsuarios);

    // MOSTRAR USUARIO INDIVIDUALK
    router.get('/usuarios/:id', usuarioControlador.mostrarUsuario);

    return router;


};

